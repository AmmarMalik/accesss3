﻿using Amazon;
using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ems.cloud
{
    class aws
    {

        /// <summary>
        /// Download a complete bucket locally
        /// </summary>
        /// <param name="accessKeyID"> Access Key to AWS account</param>
        /// <param name="secretAccessKeyID">Secret Access key to the AWS account</param>
        /// <param name="region">S3 bucket region</param>
        /// <param name="source">Bucket Name</param>
        /// <param name="destination">Local path to the folder to download</param>
        /// <returns></returns>
        public String downloadBucket(string accessKeyID, string secretAccessKeyID, string region, string source, string destination)
        {
            String retValue = String.Empty, objectName = String.Empty, errorMessage = string.Empty;
            Int32 counter = 0;
            String[] getFileName;

            try
            {
                RegionEndpoint bucketRegion = RegionEndpoint.USEast1;

                switch (region.ToUpper().Trim())
                {
                    case "US-EAST-01": { bucketRegion = RegionEndpoint.USEast1; break; }
                    case "US-WEST-01": { bucketRegion = RegionEndpoint.USWest1; break; }
                    case "US-WEST-02": { bucketRegion = RegionEndpoint.USWest2; break; }
                }

                var client = AWSClientFactory.CreateAmazonS3Client(accessKeyID, secretAccessKeyID, bucketRegion);

                // List all objects
                ListObjectsRequest listRequest = new ListObjectsRequest
                {
                    BucketName = source,
                };

                GetObjectRequest downloadSettings = new GetObjectRequest();
                downloadSettings.BucketName = source;
                counter = 0;
                ListObjectsResponse listResponse;
                do
                {
                    // Get a list of objects
                    listResponse = client.ListObjects(listRequest);
                    foreach (S3Object obj in listResponse.S3Objects)
                    {
                        if (!(obj.Key.Substring(obj.Key.Length - 1, 1) == "/"))
                        {
                            try
                            {
                                getFileName = obj.Key.Trim().Split('/');
                                objectName = getFileName[getFileName.Count() - 1];
                                downloadSettings.Key = obj.Key;
                                GetObjectResponse response = client.GetObject(downloadSettings);
                                response.WriteResponseStreamToFile(destination.Trim() + objectName);
                                counter++;
                            }
                            catch(Exception e)
                            { }
                        }
                    }

                    // Set the marker property
                    listRequest.Marker = listResponse.NextMarker;
                } while (listResponse.IsTruncated);
                retValue = "Success";
            }
            catch (Exception e)
            {
                retValue = e.Message;
            }
            return retValue;
        }

        /// <summary>
        /// Download a file in a bucket in AWS S3
        /// </summary>
        /// <param name="accessKeyID"> Access Key to AWS account</param>
        /// <param name="secretAccessKeyID">Secret Access key to the AWS account</param>
        /// <param name="region">S3 bucket region</param>
        /// <param name="source">Bucket Name</param>
        /// <param name="destination">Local path to the folder to download</param>
        /// <param name="filename"> Filename to download</param>
        /// <returns></returns>
        public String downloadFileInBucket(string accessKeyID, string secretAccessKeyID, string region, string source, string destination, string prefix, string filename)
        {
            String retValue = String.Empty, objectName = String.Empty, errorMessage = string.Empty;
            Int32 counter = 0;
            String [] getFileName;

            try
            {
                RegionEndpoint bucketRegion = RegionEndpoint.USEast1;

                switch (region.ToUpper().Trim())
                {
                    case "US-EAST-01": { bucketRegion = RegionEndpoint.USEast1; break; }
                    case "US-WEST-01": { bucketRegion = RegionEndpoint.USWest1; break; }
                    case "US-WEST-02": { bucketRegion = RegionEndpoint.USWest2; break; }
                }

                var client = AWSClientFactory.CreateAmazonS3Client(accessKeyID, secretAccessKeyID, bucketRegion);
 
                // List all objects
                ListObjectsRequest listRequest = new ListObjectsRequest
                {
                    BucketName = source,
                    Prefix = prefix,
                };

                GetObjectRequest downloadSettings = new GetObjectRequest();
                downloadSettings.BucketName = source;

                counter = 0;
                ListObjectsResponse listResponse;
                do
                {
                    // Get a list of objects
                    listResponse = client.ListObjects(listRequest);
                    foreach (S3Object obj in listResponse.S3Objects)
                    {
                        if (!(obj.Key.Substring(obj.Key.Length - 1, 1) == "/"))
                        {
                            getFileName = obj.Key.Trim().Split('/');
                            objectName = getFileName[getFileName.Count() - 1];
                            if (objectName.Trim().ToUpper() == filename.Trim().ToUpper())
                            {
                                downloadSettings.Key = obj.Key;
                                GetObjectResponse response = client.GetObject(downloadSettings);
                                response.WriteResponseStreamToFile(destination.Trim() + filename.Trim().ToUpper());
                                counter++;
                                break;
                            }
                        }
                    }

                    // Set the marker property
                    listRequest.Marker = listResponse.NextMarker;
                } while (listResponse.IsTruncated);
                retValue = "Success";
            }
            catch (Exception e)
            {
                retValue = e.Message;
            }
            return retValue;
        }

        /// <summary>
        /// Download a file in a bucket in AWS S3 with timeout
        /// </summary>
        /// <param name="accessKeyID"> Access Key to AWS account</param>
        /// <param name="secretAccessKeyID">Secret Access key to the AWS account</param>
        /// <param name="region">S3 bucket region</param>
        /// <param name="source">Bucket Name</param>
        /// <param name="destination">Local path to the folder to download</param>
        /// <param name="filename"> Filename to download</param>
        /// <param name="timeout"> Timeout in milliSeconds</param>
        /// <returns></returns>
        public String downloadFileInBucketTimeout(string accessKeyID, string secretAccessKeyID, string region, string source, string destination, string prefix, string filename, string timeout)
        {
            String retValue = String.Empty, objectName = String.Empty, errorMessage = string.Empty;
            Int32 counter = 0;
            String[] getFileName;

            try
            {
                RegionEndpoint bucketRegion = RegionEndpoint.USEast1;

                switch (region.ToUpper().Trim())
                {
                    case "US-EAST-01": { bucketRegion = RegionEndpoint.USEast1; break; }
                    case "US-WEST-01": { bucketRegion = RegionEndpoint.USWest1; break; }
                    case "US-WEST-02": { bucketRegion = RegionEndpoint.USWest2; break; }
                }

                var config = new Amazon.S3.AmazonS3Config
                {
                    ReadWriteTimeout = TimeSpan.FromMilliseconds(int.Parse(timeout)),
                    RegionEndpoint = bucketRegion,
                    MaxErrorRetry = 3
                };

                var client = AWSClientFactory.CreateAmazonS3Client(accessKeyID, secretAccessKeyID, config);

                // List all objects
                ListObjectsRequest listRequest = new ListObjectsRequest
                {
                    BucketName = source,
                    Prefix = prefix,
                };

                GetObjectRequest downloadSettings = new GetObjectRequest();
                downloadSettings.BucketName = source;

                counter = 0;
                ListObjectsResponse listResponse;
                do
                {
                    // Get a list of objects
                    listResponse = client.ListObjects(listRequest);
                    foreach (S3Object obj in listResponse.S3Objects)
                    {
                        if (!(obj.Key.Substring(obj.Key.Length - 1, 1) == "/"))
                        {
                            getFileName = obj.Key.Trim().Split('/');
                            objectName = getFileName[getFileName.Count() - 1];
                            if (objectName.Trim().ToUpper() == filename.Trim().ToUpper())
                            {
                                downloadSettings.Key = obj.Key;
                                GetObjectResponse response = client.GetObject(downloadSettings);
                                response.WriteResponseStreamToFile(destination.Trim() + filename.Trim().ToUpper());
                                counter++;
                                break;
                            }
                        }
                    }

                    // Set the marker property
                    listRequest.Marker = listResponse.NextMarker;
                } while (listResponse.IsTruncated);
                retValue = "Success";
            }
            catch (Exception e)
            {
                retValue = e.Message;
            }
            return retValue;
        }
    }
}
